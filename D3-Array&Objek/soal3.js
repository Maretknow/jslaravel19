var fruits = [];

let buah1 = {
  nama: "strawberry",
  warna: "merah",
  biji: false,
  harga: "9000",
};

let buah2 = {
  nama: "jeruk",
  warna: "oranye",
  biji: true,
  harga: "8000",
};

let buah3 = {
  nama: "semangka",
  warna: "hijau & merah",
  biji: true,
  harga: "10000",
};

let buah4 = {
  nama: "pisang",
  warna: "kuning",
  biji: false,
  harga: "5000",
};

fruits.push(buah1, buah2, buah3, buah4);

//console.log(fruits[1]);
//lihat nama buah saja
// var fruit = fruits.map(function (e) {
//   return e.nama;
// });
// fruit = fruits[0];
// var txt = "";
// for (var x in fruit) {
//   txt += fruit[x] + " ";
// }

// console.log(txt);
var biji = function (e) {
  if (e == true) {
    return "ada";
  } else {
    return "tidak";
  }
};

// for (var i = 0; i < fruits.length; i++) {
//   console.log("Nama Buah = " + fruits[i]["nama"]);
//   console.log("Warna Buah = " + fruits[i]["warna"]);
//   console.log("Ada Bijinya = " + biji(fruits[i]["biji"]));
//   console.log("Harga Buahnya =" + fruits[i]["harga"]);
// }

console.log("Nama Buah = " + fruits[0]["nama"]);
console.log("Warna Buah = " + fruits[0]["warna"]);
console.log("Ada Bijinya = " + biji(fruits[0]["biji"]));
console.log("Harga Buahnya =" + fruits[0]["harga"]);

//console.log(fruits[1]);

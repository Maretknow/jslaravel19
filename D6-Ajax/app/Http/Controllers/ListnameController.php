<?php

namespace App\Http\Controllers;

use App\Http\Resources\ListnameResource;
use Illuminate\Http\Request;
use App\Listname;

class ListnameController extends Controller
{
    public function index() {
        $listname = Listname::all();
        return ListnameResource::collection($listname);
    }

    public function create(Request $request) {
        $name = new Listname;
        $name->name = $request->name;
        $name->save();

        return new ListnameResource($name);
    }

    public function update(Request $request, $id) {
        $name = Listname::find($id);
        $name->name = $request->name;
        $name->save();

        return new ListnameResource($name);
    }

    public function delete($id){
        $name = Listname::find($id);
        $name->delete();
        return "Sucsess";
    }
}

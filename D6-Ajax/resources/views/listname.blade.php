<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>D6 | Ajak</title>
</head>

<body>
    <div id="app">
        <form action="">
            <input v-model="newName" type="text" name="nama" id="nama">
            <input v-if="update" type="submit" name="submit" id="submit" value="Update"
                v-on:click.stop.prevent="updateName(arrayke)">
            <input v-else type="submit" name="submit" id="submit" value="Add" v-on:click.stop="submitName">
        </form>
        <br>
        <ul>
            <li v-for="user in users">
               @{{user.name}} = <button v-on:click.stop="editName($index)">Edit</button> |
                <button v-on:click.stop="removeName($index, user.id)">Hapus</button>
            </li>
        </ul>
    </div>
    <script src="{{asset('/js/vue.js')}}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>
        var data = {
            newName: '',
            button: 'Submit',
            update: false,
            arrayke: ' ',
            idke: ' ',
            users: [ ],
        };

        var vo = new Vue({
            el: '#app',
            data: data,
            methods: {
                submitName: function (e) {
                    e.preventDefault();
                    if (this.newName) {
                        var name = this.newName.trim();
                        axios.post('/api/namelist', {
                                    name: name,
                                })
                                .then(responses => console.log(responses))
                        this.users.push({ name: name });
                        this.newName = ' ';
                    }
                },
                removeName: function (index, id) {
                    var name = this.users[index].name
                    if (confirm('Anda Yakin Akan Menghapus ' + name + ' ?')) {
                        axios.post('/api/namelist/'+ id +'/delete', {
                                    id: id,
                                })
                                .then(responses => console.log(responses))
                        this.users.splice(index, 1)
                    }
                },
                editName: function (index) {
                    var name = this.users[index].name;
                    this.newName = name;
                    this.update = true;
                    this.arrayke = index;
                },
                updateName: function (param, id) {
                    //console.log(param);
                    var nama = this.newName.trim();
                    if (nama) {
                        this.users[param].name = nama;
                        id = this.users[param].id;
                         axios.post('/api/namelist/'+ id, {
                                    name: nama,
                                })
                                .then(responses => console.log(responses))
                        this.newName = ' ';
                        this.update = false;
                    }
                },
            },
            created() {
                axios.get('/api/namelist').then(response => this.users = response.data.data);
            },
        });
    </script>
</body>

</html>
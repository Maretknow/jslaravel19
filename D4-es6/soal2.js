// 2. Sederhanakan menjadi Object literal di ES6
const newFunction = (firstName, lastName) => {
  return {
    firstname: firstName,
    lastname: lastName,
    fullName() {
      console.log(this.firstname + " " + this.lastname);
    },
  };
};

newFunction("Wiliam", "Imoh").fullName();
